package org.dvd.servidor;

import javax.persistence.*;
/**
 * Created by k3ym4n on 15/12/2016.
 */
@Entity
@Table (name = "lugares")
public class LugarDeInteres {

    @Id
    @GeneratedValue
    @Column ( name = "id")
    private int id;
    @Column (name = "titulo")
    private String titulo;
    @Column (name = "provincia")
    private String provincia;
    @Column (name = "localidad")
    private String localidad;
    @Column (name = "descripcion")
    private String descripcion;



    @ManyToOne
    @JoinColumn
    private Usuario usuario;



    public LugarDeInteres(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}