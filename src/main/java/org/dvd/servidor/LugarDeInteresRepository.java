package org.dvd.servidor;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by k3ym4n on 15/12/2016.
 */
public interface LugarDeInteresRepository extends CrudRepository<LugarDeInteres ,Integer> {

    List<LugarDeInteres> findAll();
}
