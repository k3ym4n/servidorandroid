package org.dvd.servidor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by k3ym4n on 15/12/2016.
 */

@RestController
public class LugarDeInteresController {

    @Autowired
    private LugarDeInteresRepository repository;

    @RequestMapping("/lugardeinteres")
    public List<LugarDeInteres> getMonumento(){
        List<LugarDeInteres> lugarDeInteres = repository.findAll();
        return  lugarDeInteres;
    }

    //id,titulo,provincia,localidad,usuario
    @RequestMapping("/addlugardeinteres")
    public void addLugarDeInteres(@RequestParam(value = "titulo" , defaultValue = "vacio") String titulo,
                                  @RequestParam (value = "provincia" , defaultValue = "vacio") String provincia,
                                  @RequestParam (value = "localidad" , defaultValue = "vacio") String localidad,
                                  @RequestParam (value = "usuario" , defaultValue = "0") Usuario usuario){
        LugarDeInteres lugarDeInteres = new LugarDeInteres();
        lugarDeInteres.setTitulo(titulo);
        lugarDeInteres.setProvincia(provincia);
        lugarDeInteres.setLocalidad(localidad);
        lugarDeInteres.setUsuario(usuario);
        repository.save(lugarDeInteres);
    }

}
